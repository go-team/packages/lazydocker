#!/bin/bash

help2man lazydocker --no-discard-stderr --version-string="$(dpkg-parsechangelog -S Version | cut -d- -f1)" -n "Lazier way to manage everything Docker" > debian/lazydocker.1
